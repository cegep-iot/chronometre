#pragma once
#include "Arduino.h"
#include "Affichage4DigitsProxy.h"

class Affichage4Digits {
    private:
        Affichage4DigitsProxy* m_proxy;
        long m_dateDernierChangement;
        bool m_termine;
        bool m_pause;
        int m_secondes;
        int m_minutes;
        void afficher(uint8_t p_d1, uint8_t p_d2, uint8_t p_p3, uint8_t p_d4);
        byte valeurSegment(int p_valeur);
        void afficherTemps(int p_secondes, int p_minutes);

    public:
        Affichage4Digits(Affichage4DigitsProxy* p_proxy);
        void tick();
        void setPause();
        void reinitialiser();
};