#pragma once
#include "Arduino.h"
#include "Action.h"
#include "Affichage4Digits.h"

class ActionBoutonReinitialiser : public Action {
    private:
        Affichage4Digits* m_affichage4Digits;

    public:
        ActionBoutonReinitialiser(Affichage4Digits* p_affichage4Digits);
        virtual void executer();
};