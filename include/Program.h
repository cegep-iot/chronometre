#pragma once
#include "Arduino.h"
#include "Affichage4Digits.h"
#include "Bouton.h"

class Program {
    private:
        Affichage4Digits* m_affichage4Digits;
        Bouton* m_boutonPause;
        Bouton* m_boutonReinitialiser;

    public:
        Program();
        void loop();
};