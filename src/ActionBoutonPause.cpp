#include "Arduino.h"
#include "ActionBoutonPause.h"
#include "Action.h"
#include "Affichage4Digits.h"

ActionBoutonPause::ActionBoutonPause(Affichage4Digits* p_affichage4Digits): m_affichage4Digits(p_affichage4Digits)
{
    ;
}

void ActionBoutonPause::executer()
{
    this->m_affichage4Digits->setPause();
}