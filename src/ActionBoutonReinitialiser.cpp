#include "Arduino.h"
#include "ActionBoutonReinitialiser.h"
#include "Action.h"
#include "Affichage4Digits.h"

ActionBoutonReinitialiser::ActionBoutonReinitialiser(Affichage4Digits* p_affichage4Digits): m_affichage4Digits(p_affichage4Digits)
{
    ;
}

void ActionBoutonReinitialiser::executer()
{
    this->m_affichage4Digits->reinitialiser();
}