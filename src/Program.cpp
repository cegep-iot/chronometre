#include "Arduino.h"
#include "Program.h"
#include "Affichage4Digits.h"
#include "Affichage4DigitsProxy.h"
#include "Affichage4DigitsProxyTM1637.h"
#include "TM1637Display.h"
#include "Bouton.h"
#include "Action.h"
#include "ActionBoutonPause.h"
#include "ActionBoutonReinitialiser.h"

Program::Program()
{
    //Affichage4DigitsProxy* proxy = new Affichage4DigitsProxyTM1637(2, 3);
    Affichage4DigitsProxy* proxy = new Affichage4DigitsProxyTM1637(26, 25);
    Affichage4Digits* affichage4Digits = new Affichage4Digits(proxy);
    Action* actionBoutonPause = new ActionBoutonPause(affichage4Digits);
    Action* actionBoutonReinitialiser = new ActionBoutonReinitialiser(affichage4Digits);
    // Bouton* boutonPause = new Bouton(5, actionBoutonPause);
    // Bouton* boutonReinitialiser = new Bouton(4, actionBoutonReinitialiser);
    Bouton* boutonPause = new Bouton(17, actionBoutonPause);
    Bouton* boutonReinitialiser = new Bouton(16, actionBoutonReinitialiser);

    this->m_affichage4Digits = affichage4Digits;
    this->m_boutonPause = boutonPause;
    this->m_boutonReinitialiser = boutonReinitialiser;
}

void Program::loop()
{
    this->m_boutonPause->tick();
    this->m_boutonReinitialiser->tick();
    this->m_affichage4Digits->tick();
}
