#include "Arduino.h"
#include "Affichage4Digits.h"
#include "Affichage4DigitsProxy.h"
#include "TM1637Display.h"

Affichage4Digits::Affichage4Digits(Affichage4DigitsProxy* p_proxy): m_proxy(p_proxy)
{
    this->m_dateDernierChangement = millis();
    this->m_termine = false;
    this->m_pause = true;
    this->m_secondes = 0;
    this->m_minutes = 0;
    this->m_proxy->afficher(0b00111111, 0b00111111, 0b00111111, 0b00111111);
}

void Affichage4Digits::afficher(uint8_t p_d1, uint8_t p_d2, uint8_t p_d3, uint8_t p_d4)
{
    this->m_proxy->afficher(p_d1, p_d2, p_d3, p_d4);
}

byte Affichage4Digits::valeurSegment(int p_valeur)
{
    byte caracteres[10] = {
        0b00111111,
        0b00000110,
        0b01011011,
        0b01001111,
        0b01100110,
        0b01101101,
        0b01111101,
        0b00000111,
        0b01111111,
        0b01101111
    };

    return caracteres[p_valeur];
}

void Affichage4Digits::afficherTemps(int p_secondes, int p_minutes)
{
    uint8_t v_d1 = valeurSegment(0);
    uint8_t v_d2 = 0;
    uint8_t v_d3 = valeurSegment(0);
    uint8_t v_d4 = 0;
    
    v_d2 = valeurSegment(p_minutes % 10);

    if(p_minutes >= 10)
    {
        p_minutes = p_minutes / 10;
        v_d1 = valeurSegment(p_minutes % 10);
    }

    v_d4 = valeurSegment(p_secondes % 10);

    if(p_secondes >= 10)
    {
        p_secondes = p_secondes / 10;
        v_d3 = valeurSegment(p_secondes % 10);
    }

    this->afficher(v_d1, v_d2, v_d3, v_d4);
}

void Affichage4Digits::tick()
{
    if(this->m_pause == false && this->m_termine == false)
    {
        long heureActuelle = millis();

        if(heureActuelle - this->m_dateDernierChangement >= 1000)
        {
            this->m_secondes++;

            if(this->m_secondes == 60)
            {
                this->m_secondes = 0;
                this->m_minutes++;

                if(this->m_minutes == 60)
                {
                    this->m_termine = true;
                }
            }

            this->m_dateDernierChangement = heureActuelle;
        }
    }

    this->afficherTemps(this->m_secondes, this->m_minutes);
}

void Affichage4Digits::setPause()
{
    if(this->m_pause == false)
    {
        this->m_pause = true;
    }
    else
    {
        this->m_pause = false;
    }
}

void Affichage4Digits::reinitialiser()
{
    this->m_minutes = 0;
    this->m_secondes = 0;
    this->afficherTemps(this->m_secondes, this->m_minutes);
    this->m_termine = false;
    this->m_pause = true;
}
